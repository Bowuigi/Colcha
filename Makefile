CC=cc
CFLAGS=-Wall -Wpedantic -Wextra -Werror -pipe -pedantic
OPTS=-O3

build:
	$(CC) $(CFLAGS) $(OPTS) colcha.c -o colcha

debug:
	$(CC) $(CFLAGS) -ggdb colcha.c -o colcha

