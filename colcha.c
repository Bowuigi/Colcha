/*
 * Colcha: Lenguaje de programación basado en la ejecución de imagenes Farbfeld mediante su color.
*/

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <arpa/inet.h>

#define RGB_HALF             (UINT16_MAX/2)
#define RGB_HALF_TOP_HALF    RGB_HALF + RGB_HALF/2
#define RGB_HALF_BOTTOM_HALF RGB_HALF - RGB_HALF/2

enum instr {
	PUT,   // Imprimir el valor de la celda actual como un 'unsigned char'
	GET,   // Conseguir un caracter de 'stdin' y almacenarlo en la celda actual
	PREV,  // Mueve la celda actual a la anterior
	NEXT,  // Mueve la celda actual a la siguiente
	INC,   // Suma uno al valor de la celda actual
	DEC,   // Resta uno al valor de la celda actual
	RIGHT, // Gira a la derecha si el valor de la celda actual es 0
	LEFT,  // Gira a la izquierda si el valor de la celda actual es 0
	NOP    // No hace nada, por lo visto es importante
};

enum mask {
	MASK_R = 1,
	MASK_G = 2,
	MASK_B = 4
};

enum rotation {
	ROT_RIGHT = 0,
	ROT_DOWN = 1,
	ROT_LEFT = 2,
	ROT_UP = 3,
};

int main(int argc, char **argv) {
	/* Argumentos */
	if (argc != 2) {
		fputs("[Colcha] Información de uso: colcha imagen.ff\n", stderr);
		return 1;
	}

	/* Cargar imagen Farbfeld */
	FILE *fp = fopen(argv[1], "r");
	char magic[9];
	uint32_t width = 0;
	uint32_t height = 0;
	enum instr *map;
	uint16_t rgba[4];

	if (fread(magic, sizeof(*magic), 8, fp) != 8) {
		fputs("[Colcha] Error: No se pudo leer la imagen\n", stderr);
		return 1;
	}

	if (strcmp(magic, "farbfeld")) {
		fputs("[Colcha] Error: El archivo dado no es una imagen en formato Farbfeld\n", stderr);
		return 1;
	}

	if (fread(&width, sizeof(width), 1, fp) != 1) {
		fputs("[Colcha] Error: No se pudo leer la imagen\n", stderr);
		return 1;
	}

	width = be32toh(width);

	if (fread(&height, sizeof(width), 1, fp) != 1) {
		fputs("[Colcha] Error: No se pudo leer la imagen\n", stderr);
		return 1;
	}
	height = be32toh(height);

	map = malloc(width*height * sizeof(*map));

	if (!map) {
		fputs("[Colcha] Error: Memoria insuficiente\n", stderr);
		return 1;
	}

	/* Convertirla en un mapa de instrucciones */
	for (uint32_t i = 0; i < width*height; i++) {
		if (fread(&rgba, sizeof(*rgba), 4, fp) != 4) {
			fputs("[Colcha] Error: No se pudo leer la imagen\n", stderr);
			return 1;
		}

		rgba[0] = be16toh(rgba[0]);
		rgba[1] = be16toh(rgba[1]);
		rgba[2] = be16toh(rgba[2]);

		uint8_t mask = 0;
		if (rgba[0] > RGB_HALF)
			mask |= MASK_R;

		if (rgba[1] > RGB_HALF)
			mask |= MASK_G;

		if (rgba[2] > RGB_HALF)
			mask |= MASK_B;

		switch (mask) {
			case 0:
				if (rgba[0] > RGB_HALF_BOTTOM_HALF && rgba[0] < RGB_HALF_TOP_HALF) {
					map[i] = NOP;
				} else {
					map[i] = PUT;
				}
				break;
			case MASK_R:
				map[i] = GET;
				break;
			case MASK_G:
				map[i] = PREV;
				break;
			case MASK_B:
				map[i] = NEXT;
				break;
			case (MASK_R | MASK_G):
				map[i] = INC;
				break;
			case (MASK_G | MASK_B):
				map[i] = DEC;
				break;
			case (MASK_R | MASK_B):
				map[i] = RIGHT;
				break;
			case (MASK_R | MASK_G | MASK_B):
				if (rgba[0] > RGB_HALF_BOTTOM_HALF && rgba[0] < RGB_HALF_TOP_HALF) {
					map[i] = NOP;
				} else {
					map[i] = LEFT;
				}
				break;
		}
	}

	/* Ejecutar programa */
	uint16_t *tape;
	size_t tape_size = 8;
	uint32_t tape_index = 0;
	uint32_t x = 0;
	uint32_t y = 0;
	enum rotation rot = ROT_RIGHT;
	bool running = true;

	tape = malloc(tape_size * sizeof(*tape));

	if (!tape) {
		fputs("[Colcha] Error: Memoria insuficiente\n", stderr);
		return 1;
	}

	while (running) {
		switch (map[y*width+x]) {
			case PUT:
				fputc((unsigned char) tape[tape_index], stdout);
				break;
			case GET:
				tape[tape_index] = (uint16_t) fgetc(stdin);
				break;
			case PREV:
				if (tape_index == 0)
					running = false;

				tape_index--;
				break;
			case NEXT:
				tape_index++;

				if (tape_index > tape_size) {
					tape_size <<= 1;
					void *tmp = realloc(tape, tape_size);
					if (!tmp) {
						free(map);
						fputs("[Colcha] Error: Memoria insuficiente\n", stderr);
						return 1;
					}
					tape = tmp;
				}
				break;
			case INC:
				tape[tape_index]++;
				break;
			case DEC:
				tape[tape_index]--;
				break;
			case RIGHT:
				if (tape[tape_index])
					rot = (rot + 1) & 3;
				break;
			case LEFT:
				if (tape[tape_index])
					rot = (rot - 1) & 3;
				break;
			case NOP:
				break;
		}

		switch (rot) {
			case ROT_UP:
				y = (y - 1) % height;
				break;
			case ROT_DOWN:
				y = (y + 1) % height;
				break;
			case ROT_LEFT:
				x = (x - 1) % width;
				break;
			case ROT_RIGHT:
				x = (x + 1) % width;
				break;
		}
	}

	/* Finalizar */
	fflush(stdout);
}

