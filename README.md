# Colcha

Un lenguaje de programación esotérico (esolang) basado en la colcha del libro "Como agua para chocolate" de Laura Esquivel

La idea es que cada programa se vea como la colcha que Tita hizo a mano, de gran tamaño y muy colorida.

Colcha es un lenguaje de dos dimensiones, utilizando la imagen dada en formato Farbfeld como el programa a ejecutar.

Colcha tiene 9 instrucciones, cada una definida por el color de el pixel (se redondean) sobre el que se encuentra el puntero de instrucciones:

<table>
	<tr>
		<th>Instrucción</th>
		<th>Color</th>
		<th>Descripción</th>
	</tr>
	<tr>
		<td>PUT</td>
		<td>Negro</td>
		<td>Imprimir el valor de la celda actual como un 'unsigned char'</td>
	</tr>
	<tr>
		<td>GET</td>
		<td>Rojo</td>
		<td>Conseguir un caracter de la entrada estandar (stdin) y almacenarlo en la celda actual</td>
	</tr>
	<tr>
		<td>PREV</td>
		<td>Verde</td>
		<td>Mueve la celda actual a la anterior, finaliza el programa si se trata de mover antes de la celda 0</td>
	</tr>
	<tr>
		<td>NEXT</td>
		<td>Azul</td>
		<td>Mueve la celda actual a la siguiente</td>
	</tr>
	<tr>
		<td>INC</td>
		<td>Amarillo</td>
		<td>Suma uno al valor de la celda actual</td>
	</tr>
	<tr>
		<td>DEC</td>
		<td>Cyan</td>
		<td>Resta uno al valor de la celda actual</td>
	</tr>
	<tr>
		<td>RIGHT</td>
		<td>Magenta</td>
		<td>Gira a la derecha si el valor de la celda actual es 0</td>
	</tr>
	<tr>
		<td>LEFT</td>
		<td>Blanco</td>
		<td>Gira a la izquierda si el valor de la celda actual es 0</td>
	</tr>
	<tr>
		<td>NOP</td>
		<td>Gris</td>
		<td>No hace nada, por lo visto es importante</td>
	</tr>
</table>

Colcha posee una cinta de celdas infinitas (siempre y cuando la computadora tenga memoria suficiente), cada celda tiene 16 bits (0-65535) y no tiene signo, por lo que al llegar a uno de los límites simplemente "da la vuelta" (wrap-around).

No se ha probado que Colcha pase el test computacional de Turing (para ser capaz de programar cualquier cosa con Colcha), pero se supone que debería pasarlo sin problemas.

## Ejemplos

![Imprimir la letra C en Colcha](ejemplos/c-scaled.png)

Imprimir la letra 'C', escalado varias veces para obtener una imagen visible

## Instalación

Para Linux, MacOS y otros derivados de UNIX: `make` en la línea de comandos compila el programa, mientras que 'sudo make install' (reemplazando `sudo` apropiadamente, como por `boas` o `doas`) lo instala.

Aun no existe una forma de instalarlo en Windows sin utilizar MinGW o similares.

## Uso

`colcha imagen.ff`

